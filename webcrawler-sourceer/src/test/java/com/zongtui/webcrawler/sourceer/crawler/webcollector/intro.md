WebCollector is an open source web crawler framework based on Java.It provides some simple interfaces for crawling the Web,you can setup a multi-threaded web crawler in less than 5 minutes.

1.x：

WebCollector 1.x版本现已转移到http://git.oschina.net/webcollector/WebCollector-1.x维护，建议使用2.x版本。

2.x：

WebCollector 2.x版本特性：

1）自定义遍历策略，可完成更为复杂的遍历业务，例如分页、AJAX
2）内置Berkeley DB管理URL，可以处理更大量级的网页
3）集成selenium，可以对javascript生成信息进行抽取
4）直接支持多代理随机切换
5）集成spring jdbc和mysql connector，方便数据持久化
6）集成json解析器
7）使用slf4j作为日志门面
8）修改http请求接口，用户自定义http请求更加方便