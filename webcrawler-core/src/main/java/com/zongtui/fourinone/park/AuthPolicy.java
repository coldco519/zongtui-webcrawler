package com.zongtui.fourinone.park;

public enum AuthPolicy {
    OP_READ(1), OP_READ_WRITE(3), OP_ALL(7);
    private int policy;

    AuthPolicy(int policy) {
        this.policy = policy;
    }

    public int getPolicy() {
        return policy;
    }

    public static boolean authIncluded(int targetAuth, int curAuth) {
        return ((targetAuth & curAuth) == targetAuth);
    }

    public static void main(String[] args) {
        System.out.println(authIncluded(AuthPolicy.OP_ALL.getPolicy(), AuthPolicy.OP_READ_WRITE.getPolicy()));//ParkAuth.OP_READ
    }

}